package Entities;

public class Ball extends Entity implements Cloneable{
    private static int count = 0;

    private boolean isInRightHole;

    public boolean isInRightHole() {
        return isInRightHole;
    }

    public void setInRightHole(boolean inRightHole) {
        isInRightHole = inRightHole;
    }

    public Ball() {
        this.id = ++count;
    }

    @Override
    public Ball clone() {
        try {
            return (Ball)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ball ball = (Ball) o;

        return id == ball.id;
    }

    @Override
    public int hashCode() {
        return (isInRightHole ? 1 : 0);
    }
}
