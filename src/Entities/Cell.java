package Entities;

import java.util.ArrayList;

public class Cell {
    private ArrayList<Entity> content = new ArrayList<>();
    private Coords coords;

    public Cell(Coords coords) {
        this.coords = coords;
    }

    public ArrayList<Entity> getContent() {
        return content;
    }

    public boolean addContent(Entity entity) {
        if (content.size() > 0) {
            for (Entity curEntity : content) {
                if (entity.getClass().equals(curEntity.getClass())) {
                    return false;
                }
            }
        }
        content.add(entity);
        return true;
    }

    public Entity getBall() {
        if (content.size() > 0) {
            for (Entity curEntity : content) {
                if (curEntity instanceof Ball) {
                    return curEntity;
                }
            }
        }
        return null;
    }

    public boolean isContainedBall() {
        return this.getBall() != null;
    }

    public Entity getHole() {
        if (content.size() > 0) {
            for (Entity curEntity : content) {
                if (curEntity instanceof Hole) {
                    return curEntity;
                }
            }
        }
        return null;
    }

    public boolean isContainedHole() {
        return this.getHole() != null;
    }

    public Entity getWall() {
        if (content.size() > 0) {
            for (Entity curEntity : content) {
                if (curEntity instanceof Wall) {
                    return curEntity;
                }
            }
        }
        return null;
    }

    public boolean isContainedWall() {
        return this.getWall() != null;
    }

    public Coords getCoords() {
        return coords;
    }
}
