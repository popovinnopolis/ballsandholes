package Entities;

public class Coords implements Cloneable{
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coords coords = (Coords) o;

        if (x != coords.x) return false;
        return y == coords.y;
    }

    @Override
    public int hashCode() {
        return 31 * x + y;
    }

    @Override
    public Coords clone() {
        try {
            return (Coords)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public int moveX(int x) {
        this.x += x;
        return this.x;
    }

    public int moveY(int y) {
        this.y += y;
        return this.y;
    }

    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }

    public Coords moved(int x, int y) {
        return new Coords(this.x + x, this.y + y);
    }
}
