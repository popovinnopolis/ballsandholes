package Entities;

import java.util.ArrayList;
import java.util.HashMap;

import static Utils.BoardUtils.isCorrectCoords;

public class Board {
    private int size;
    private boolean isWin = true;
    private String path = "";
    private ArrayList<ArrayList<Cell>> board = new ArrayList<>(size);
    private HashMap<Ball, Coords> balls = new HashMap<>();
    private HashMap<Hole, Coords> holes = new HashMap<>();
    private HashMap<Wall, Coords> walls = new HashMap<>();

    public Board(int size) {
        this.size = size;
        for (int y = 1; y <= size; y++) {
            ArrayList<Cell> row = new ArrayList<>();
            for (int x = 1; x <= size; x++) {
                Cell cell = new Cell(new Coords(x, y));
                row.add(cell);
            }
            board.add(row);
        }
    }

    public Board(int size, HashMap<Ball, Coords> balls, HashMap<Hole, Coords> holes, HashMap<Wall, Coords> walls) {
        this(size);
        for (Ball ball : balls.keySet()) {
            addEntity(balls.get(ball), ball);
        }
        for (Hole hole : holes.keySet()) {
            addEntity(holes.get(hole), hole);
        }
        for (Wall wall : walls.keySet()) {
            addEntity(walls.get(wall), wall);
        }
    }

    public boolean addEntity(Coords coords, Entity entity) {
        Cell cell = this.getCell(coords);
        if (!cell.addContent(entity)) return false;
        if (entity instanceof Ball) {
            balls.put((Ball) entity, coords);
            if (!((Ball) entity).isInRightHole()) isWin = false;
        }
        if (entity instanceof Hole) holes.put((Hole) entity, coords);
        if (entity instanceof Wall) walls.put((Wall) entity, coords);
        return true;
    }

    public Cell getCell(Coords coords) {
        return board.get(coords.getY() - 1).get(coords.getX() - 1);
    }

    public int getSize() {
        return size;
    }

    public Board tilt(Direction direction) {
        HashMap<Ball, Coords> newBalls = new HashMap<>();
        for (Ball cBall : balls.keySet()) {
            Ball ball = cBall.clone();
            Coords coords = getBallPosition(ball);
            //TODO Sort balls by positions depends directions of tilt
            Coords newCoords = moveBall(ball, direction);
            newBalls.put(ball, newCoords);
        }
        Board newBoard = new Board(getSize(), newBalls, holes, walls);
        newBoard.setPath(getPath() + direction);
        return newBoard;
    }

    private Coords moveBall(Ball ball, Direction direction) {
        Coords before = getBallPosition(ball);
        if (ball.isInRightHole()) return before;
        Coords after = before.clone();
        int offsetX = 0, offsetY = 0;
        switch (direction) {
            case E: {
                offsetX = 1;
                break;
            }
            case W: {
                offsetX = -1;
                break;
            }
            case N: {
                offsetY = -1;
                break;
            }
            case S: {
                offsetY = 1;
                break;
            }
        }

        while (isLegalPosition(ball, after.moved(offsetX, offsetY))) {
            if (getCell(after).isContainedWall() && getCell(after.moved(offsetX, offsetY)).isContainedWall() &&
                    getCell(after).getWall().id == getCell(after.moved(offsetX, offsetY)).getWall().id) {
                // collision with wall
                break;
            }
            after.move(offsetX, offsetY);
            Cell cell = getCell(after);
            Hole hole = (Hole) cell.getHole();
            if (hole != null && hole.getId() != ball.getId()) {
                // in incorrect hole
                after = null;
                break;
            } else if (hole != null && hole.getId() == ball.getId()) {
                // in correct hole
                ball.setInRightHole(true);
                break;
            }
        }
        return after;
    }

    private boolean isLegalPosition(Ball ball, Coords coords) {
        if (!isCorrectCoords(coords, size)) return false;
        if (ball.isInRightHole() && !getBallPosition(ball).equals(coords)) return false;
        if (getCell(coords).isContainedBall() && !((Ball) getCell(coords).getBall()).isInRightHole()) return false;
        return true;
    }

    private Coords getBallPosition(Ball ball) {
        return balls.get(ball);
    }

    private Coords getHolePosition(Hole hole) {
        return holes.get(hole);
    }

    private Coords getWallPosition(Wall wall) {
        return walls.get(wall);
    }

    public boolean isWin() {
        return isWin;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
