package Utils;

import Entities.*;

public class BoardUtils {

    public static Board CreateBoard() {
        Board board = new Board(Loader.getBoardSize());

        for (int i = 1; i <= Loader.getNumHolesNBalls(); i++) {
            board.addEntity(Loader.getBall(i), new Ball());
            board.addEntity(Loader.getHole(i), new Hole());
        }

        for (int i = 1; i <= Loader.getNumWalls(); i++) {
            Coords[] coords = Loader.getWall(i);

            Wall.Orientation orientation = coords[0].getX() == coords[1].getX() ? Wall.Orientation.horizontal :
                    Wall.Orientation.vertical;
            Wall wall1 = new Wall(orientation);
            Wall wall2 = new Wall(orientation);
            wall2.setId(wall1.getId());
            board.addEntity(coords[0], wall1);
            board.addEntity(coords[1], wall2);
        }
        return board;
    }

    public static void showBoard(Board board) {
        for (int y = -1; y <= board.getSize(); y++) {
            StringBuilder row = new StringBuilder();

            Cell cell;
            for (int x = 0; x <= board.getSize(); x++) {
                if (y < 1) {
                    // draw Header
                    if (y == -1 && x != 0) row.append(String.format("\t %d \t", x));
                    if (y == 0 && x == 0) row.append("\t");
                    if (y == 0 && x != 0) row.append("-------");
                } else {
                    // draw Board
                    if (x == 0) {
                        row.append(String.format("%d |", y));
                        continue;
                    }
                    cell = board.getCell(new Coords(x,y));
                    String item = "";
                    if (cell.isContainedBall()) {
                        item += String.format("B%d", cell.getBall().getId());
                    }
                    if (cell.isContainedHole()) {
                        item += String.format("H%d", cell.getHole().getId());
                    }
                    if (cell.isContainedWall()) {
                        item += String.format("W%d", cell.getWall().getId());
                    }

                    row.append(String.format("\t%s\t", item));
                }
            }
            System.out.println(row);
        }
        if (board.isWin()) System.out.println("Win!");
        System.out.printf("\tPath: %s\n", board.getPath());
    }

    public static boolean isCorrectCoords(Coords coords, int size) {
        return (coords.getX() >= 1 && coords.getX() <= size && coords.getY() >= 1 && coords.getY() <= size);
    }

    public static Board tiltBoard(Board board, Direction direction) {
        return board.tilt(direction);
    }
}
