import Entities.Board;
import Entities.Direction;
import Utils.Loader;
import Utils.BoardUtils;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;

public class Main {
    private static Queue<Board> boards = new ArrayDeque<>();

    public static void main(String[] args) throws IOException {
        if (args.length < 1) return;

        Loader.load(args[0]);
        Board board = BoardUtils.CreateBoard();
        Board newBoard;
        boards.add(board);
        while (!boards.isEmpty()) {
            board = boards.remove();
            for (Direction direction : Direction.values()) {
                if (board.getPath().length() > 2) return;
                newBoard = BoardUtils.tiltBoard(board, direction);
                if (newBoard.isWin()) {
                    // find solution
                    System.out.println();
                    BoardUtils.showBoard(newBoard);
                    return;
                }
                boards.add(newBoard);
            }
        }
    }
}
